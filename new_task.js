#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn){
  if (err){ console.log('error', err);}
  // Creating channel, where most of the API for getting things done resides
  conn.createChannel(function(err, ch){
    var q = 'task_queue';
    var msg = process.argv.slice(2).join(' ') || "Hello World!";

    ch.assertQueue(q, {durable: true});
    ch.sendToQueue(q, new Buffer(msg), {persistent: true});
    console.log(" [x] Sent '%s'", msg);

  });

  // Close connection
  setTimeout(function() {
    conn.close();
    process.exit(0)
  }, 500);
})
