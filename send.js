#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn){
  if (err){ console.log('error', err);}
  // Creating channel, where most of the API for getting things done resides
  conn.createChannel(function(err, ch){
    var q = 'hello';

    ch.assertQueue(q, { durable: false });
    ch.sendToQueue(q, new Buffer('Hello World'));
    console.log("[x] Sent 'Hello World!'");

  });

  // Close connection
  setTimeout(function() {
    conn.close();
    process.exit(0)
  }, 500);
})
